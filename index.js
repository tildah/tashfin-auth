const tashfinCRUD = require("tashfin-crud")
const { parseBody, resJSON } = require("tashfin");
const { ObjectId } = require("mongodb");
const jwt = require('jsonwebtoken');
const { parse, serialize } = require('cookie');
const crypto = require("crypto");

const transformPwd = (pwd) =>  {
  const hash = crypto.createHash("sha256");
  hash.update(pwd);
  return hash.digest("hex");
}

const passwordType = {
  name: "password",
  coerce: transformPwd,
  validate: value => value && value.length > 6
}

module.exports = (moduleName, schema, db, moduleOptions) => {

  const crudOptions = {
    customTypes: [ ...(moduleOptions.customTypes || []), passwordType ],
    ...moduleOptions
  }

  const crud = tashfinCRUD(moduleName, schema, db, crudOptions);

  const {
    maxAge = 86400000,
    secret,
    algorithm,
    autoRefresh = true,
    loginOnRegister = true,
    getUserVisa = () => [],
  } = moduleOptions;

  const createToken = user => {
    const token = jwt.sign(user, secret, { expiresIn: maxAge, algorithm })
    const cookieOpts = { httpOnly: true, maxAge }
    return {
      "Set-Cookie": serialize("Authorization", `Bearer ${token}`, cookieOpts),
    }
  }
  
  const withAuth = async (req, fn) => {
    try {
      const cookies = parse(req.headers.cookie || "");
      const token = cookies.Authorization.replace("Bearer ");
      const user = jwt.verify(token, secret, { algorithm })
      return await fn(user);
    } catch(e) {
      return { statusCode: 401, content: e.message }
    }
  }

  const withAccess = async (req, access, fn) =>
    await withAuth(req, async user => {
      const visa = getUserVisa(user);
      if(!visa.includes(access)) return { statusCode: 403 }
      return await fn(user);
    }
  )

  const POST_register = async req => {
    const { password, ...user } = await crud.POST_root(req);
    if (loginOnRegister) return createToken(user);
    return { statusCode: 204 }
  }

  const POST_login = async req => {
    const { email, password } = parseBody(req);
    if (!email || !password)
      return { statusCode: 400, content: "Missing Credentials" };
    const filter = { email, password: transformPwd(password) }
    const fields = { password: 0 };
    const user = await db.collection(moduleName).findOne(filter, fields);
    if (!user) return { statusCode: 401, content: "Invalid Credentials" };
    return createToken(user);
  }

  const GET_me = async req => await withAuth(req, user => ({
    ...resJSON(user),
    ...(autoRefresh ? createToken : {})
  }));

  return { ...crud, POST_login, GET_me, withAuth }
}
